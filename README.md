SDCSurvey
=========

Ứng dụng Khảo sát sinh viên dành riêng cho Trường ĐH Văn Lang
Phiên bản 4.2.3
------------------
* Chặn các trình duyệt IE 8 trở xuống sử dụng
* Cập nhật quyền kết xuất dữ liệu
* Cập nhật giao diện Nhóm người dùng
* Cập nhật giao diện Phân quyền Nhóm người dùng
* Cập nhật giao diện Quản lý Nhóm người dùng
* Cập nhật giao diện Tài khoản người dùng (Thêm, Sửa)


Phiên bản 4.2.2
------------------
* Bắt lỗi nhập đúng dạng chuẩn của email
* Sửa lỗi không hiện modal popup trong phần create summary  của phiếu khảo sát
* Cập nhật hiển thị sai icon trong danh sách các phiếu khảo sát (Phiếu khảo sát đang được sử dụng)
* Cập nhật alert trong import student


Phiên bản 4.2.1
------------------
* Cập nhật CodeIgniter Framework 2.2.0 (16/6/2014)
* Cập nhật  lại các thư viện JQuery 2.x, Bootrap, Highchart 3.0.10, và một số thư viện khác
* Cập nhật lại giao diện (UI) các trang:
	+ Login
	+ Dashboard
	+ Import Student
	+ Export Student
	+ Survey type (list, edit)
	+ Survey (list, create step1, edit step 1, edit step 2, edit step 3, edit step 4 …)
		- List, create step 1, edit step 1, edit step 2
		- Edit step 3 (16/6/2014)
		- Edit step 4 (20/6/2014)
		- Edit effect (20/6/2014)
	+ Thông báo
	+ Sort questions
	+ Sort answer
* Cập nhật DateTime runtime
* Thêm plugin Pace ( trạng thái loading )


Phiên bản 4.1.0.x
------------------
* Thêm tính năng xác định câu hỏi có việc làm / chưa có việc làm trong phần thêm câu hỏi
* Thêm tính năng cập nhật có việc làm / chưa có việc làm trong phần cập nhật câu hỏi
* Cập nhật thư viện jquery 1.11.0
* Sửa lỗi #1
